## Local Argo

```brew install argo```

```argo server```

## Useful Resources

- [kubectl cheatsheet](https://kubernetes.io/docs/reference/kubectl/cheatsheet/)


## Temporary work-around

1. Deploy from quickstart [as described here](https://argoproj.github.io/argo-workflows/quick-start/) and port forward `argo-server`.
2. Download firefox (Firefox lets you bypass insecure https warning)
3. Ignore https in the python client

```python
import yaml
import requests
from argo.workflows.client import (ApiClient,
                                   WorkflowServiceApi,
                                   Configuration,
                                   V1alpha1WorkflowCreateRequest)

# assume we ran `kubectl -n argo port-forward deployment/argo-server 2746:2746`

config = Configuration(host="https://localhost:2746")
config.verify_ssl = False
client = ApiClient(configuration=config)
service = WorkflowServiceApi(api_client=client)
WORKFLOW = 'https://raw.githubusercontent.com/argoproj/argo/v2.12.2/examples/dag-diamond-steps.yaml'

resp = requests.get(WORKFLOW)
manifest: dict = yaml.safe_load(resp.text)

service.create_workflow('argo', V1alpha1WorkflowCreateRequest(workflow=manifest))
```

# Other useful stuff

```
kubectl apply -n argo -f https://raw.githubusercontent.com/argoproj/argo-workflows/stable/manifests/quick-start-postgres.yaml | jq '.spec.template.spec.containers[0].env |= .+ {"name": "ARGO_SECURE", "value": false}'
```

get logs of a pod/deployment

```
kubectl logs -f -n argo deployment/argo-server
```


jq / jy

```
 kubectl get deployment argo-server -n argo -o json | jq '.spec.template.spec.containers[0].args |= .+ ["--secure=false"]' | kubectl replace -f -
```