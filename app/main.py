import yaml
import requests
from argo.workflows.client import (ApiClient,
                                   WorkflowServiceApi,
                                   Configuration,
                                   V1alpha1WorkflowCreateRequest)

# assume we ran `kubectl -n argo port-forward deployment/argo-server 2746:2746`

config = Configuration(host="https://localhost:2746")
config.verify_ssl = False  # Set verify_ssl to false until we figure out how to get argo to host without https
client = ApiClient(configuration=config)
service = WorkflowServiceApi(api_client=client)
WORKFLOW = 'https://raw.githubusercontent.com/argoproj/argo/v2.12.2/examples/dag-diamond-steps.yaml'

resp = requests.get(WORKFLOW)
manifest: dict = yaml.safe_load(resp.text)

service.create_workflow('argo', V1alpha1WorkflowCreateRequest(workflow=manifest))